/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

namespace Trk {
/** Use the Surface as a ParametersBase constructor, from local parameters -
 * charged */
inline ParametersT<5, Charged, PerigeeSurface>*
PerigeeSurface::createTrackParameters(double l1,
                                      double l2,
                                      double phi,
                                      double theta,
                                      double qop,
                                      AmgSymMatrix(5) * cov) const
{
  return new ParametersT<5, Charged, PerigeeSurface>(
    l1, l2, phi, theta, qop, *this, cov);
}

/** Use the Surface as a ParametersBase constructor, from local parameters -
 * charged */
inline Surface::ChargedTrackParametersUniquePtr
PerigeeSurface::createUniqueTrackParameters(double l1,
                                      double l2,
                                      double phi,
                                      double theta,
                                      double qop,
                                      AmgSymMatrix(5) * cov) const
{
  return std::make_unique<ParametersT<5, Charged, PerigeeSurface>>(
    l1, l2, phi, theta, qop, *this, cov);
}

inline Surface::ChargedTrackParametersUniquePtr
PerigeeSurface::createUniqueTrackParameters(const Amg::Vector3D& position,
                                      const Amg::Vector3D& momentum,
                                      double charge,
                                      AmgSymMatrix(5) * cov) const
{
  return std::make_unique<ParametersT<5, Charged, PerigeeSurface>>(
    position, momentum, charge, *this, cov);
}


/** Use the Surface as a ParametersBase constructor, from global parameters -
 * charged*/
inline ParametersT<5, Charged, PerigeeSurface>*
PerigeeSurface::createTrackParameters(const Amg::Vector3D& position,
                                      const Amg::Vector3D& momentum,
                                      double charge,
                                      AmgSymMatrix(5) * cov) const
{
  return new ParametersT<5, Charged, PerigeeSurface>(
    position, momentum, charge, *this, cov);
}

/** Use the Surface as a ParametersBase constructor, from local parameters -
 * neutral */
inline ParametersT<5, Neutral, PerigeeSurface>*
PerigeeSurface::createNeutralParameters(double l1,
                                        double l2,
                                        double phi,
                                        double theta,
                                        double qop,
                                        AmgSymMatrix(5) * cov) const
{
  return new ParametersT<5, Neutral, PerigeeSurface>(
    l1, l2, phi, theta, qop, *this, cov);
}

/** Use the Surface as a ParametersBase constructor, from global parameters -
 * neutral */
inline ParametersT<5, Neutral, PerigeeSurface>*
PerigeeSurface::createNeutralParameters(const Amg::Vector3D& position,
                                        const Amg::Vector3D& momentum,
                                        double charge,
                                        AmgSymMatrix(5) * cov) const
{
  return new ParametersT<5, Neutral, PerigeeSurface>(
    position, momentum, charge, *this, cov);
}

/** Use the Surface as a ParametersBase constructor, from local parameters */
template<int DIM, class T>
ParametersT<DIM, T, PerigeeSurface>*
PerigeeSurface::createParameters(double l1,
                                 double l2,
                                 double phi,
                                 double theta,
                                 double qop,
                                 AmgSymMatrix(DIM) * cov) const
{
  return new ParametersT<DIM, T, PerigeeSurface>(
    l1, l2, phi, theta, qop, *this, cov);
}

/** Use the Surface as a ParametersBase constructor, from global parameters */
template<int DIM, class T>
ParametersT<DIM, T, PerigeeSurface>*
PerigeeSurface::createParameters(const Amg::Vector3D& position,
                                 const Amg::Vector3D& momentum,
                                 double charge,
                                 AmgSymMatrix(DIM) * cov) const
{
  return new ParametersT<DIM, T, PerigeeSurface>(
    position, momentum, charge, *this, cov);
}

/** Return the surface type */
inline Surface::SurfaceType
PerigeeSurface::type() const
{
  return PerigeeSurface::staticType;
}

inline PerigeeSurface*
PerigeeSurface::clone() const
{
  return new PerigeeSurface(*this);
}

inline const Amg::Vector3D&
PerigeeSurface::normal() const
{
  return s_xAxis;
}


inline const Amg::Vector3D*
PerigeeSurface::normal(const Amg::Vector2D&) const
{
  return new Amg::Vector3D(normal());
}

inline bool
PerigeeSurface::insideBounds(const Amg::Vector2D&, double, double) const
{
  return true;
}

inline bool
PerigeeSurface::insideBoundsCheck(const Amg::Vector2D&,
                                  const BoundaryCheck&) const
{
  return true;
}

inline bool
PerigeeSurface::isOnSurface(const Amg::Vector3D&,
                            BoundaryCheck,
                            double,
                            double) const
{
  return true;
}

inline const NoBounds&
PerigeeSurface::bounds() const
{
  return s_perigeeBounds;
}

inline Intersection
PerigeeSurface::straightLineIntersection(const Amg::Vector3D& pos,
                                         const Amg::Vector3D& dir,
                                         bool forceDir,
                                         Trk::BoundaryCheck) const
{
  // following nominclature found in header file and doxygen documentation
  // line one is the straight track
  const Amg::Vector3D& ma = pos;
  const Amg::Vector3D& ea = dir;
  // line two is the line surface
  const Amg::Vector3D& mb = center();
  const Amg::Vector3D& eb = lineDirection();
  // now go ahead
  Amg::Vector3D mab(mb - ma);
  double eaTeb = ea.dot(eb);
  double denom = 1 - eaTeb * eaTeb;
  if (fabs(denom) > 10e-7) {
    double lambda0 = (mab.dot(ea) - mab.dot(eb) * eaTeb) / denom;
    // evaluate the direction, bounds are always true for Perigee
    bool isValid = forceDir ? (lambda0 > 0.) : true;
    return Trk::Intersection((ma + lambda0 * ea), lambda0, isValid);
  }
  return Trk::Intersection(pos, 0., false);
}

inline const Amg::Vector3D&
PerigeeSurface::lineDirection() const
{
  if (m_lineDirection.isValid()) {
    return *(m_lineDirection.ptr());
  }

  if (Surface::m_transform) {

    if (!m_lineDirection.isValid()) {
      m_lineDirection.set(transform().rotation().col(2));
      return *(m_lineDirection.ptr());
    }
  }
  return Trk::s_zAxis;
}
/** the pathCorrection for derived classes with thickness */
inline double
PerigeeSurface::pathCorrection(const Amg::Vector3D&, const Amg::Vector3D&) const
{
  return 1.;
}

/** Return properly formatted class name for screen output */
inline std::string
PerigeeSurface::name() const
{
  return "Trk::PerigeeSurface";
}

}
